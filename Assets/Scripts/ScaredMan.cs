﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using System;

public class ScaredMan : Agent
{
    public Transform targetT;
    int startInt = 0;
    bool canJump = false;
    int counter = 0;
    //Es como un start
    public override void OnEpisodeBegin()
    {
        counter = 0;
        //No quiero que salte en el aire
        canJump = false;
        //Respawn de la CPU
        this.transform.position = new Vector3(2f, 1.25f, -3f);
        //Respawn del zombie (es aleatorio)
        GameObject.Find("Zombie").GetComponent<RespawnRandom>().respawn();
    }

    public override void CollectObservations(VectorSensor sensor)
    {

        sensor.AddObservation(transform.position);
        //En vez de que vaya a por el, la CPU huira de el. Aun asi le añado su observacion para saber que es ese zombie, si algo bueno, o algo malo.
        sensor.AddObservation(targetT.position);
        //Solo entrara una vez. Ni siquiera entrara si le damos un EndEpisode.
        if (startInt <= 0)
        {
            Debug.Log(startInt);
            StartCoroutine(roundsTime());
            startInt++;
        }
        

    }

    IEnumerator roundsTime()
    {
        
        Debug.Log("entro");
        
        yield return new WaitForSeconds(1f);
        //Sumara cada segundo. y si pasan los 60 segundos, se acaba el episodio y recibe puntos.
        //Por que hago un wait de 1s y sumo cada segundo y no hago solo un wait de 60 segundos??? porque si te mueres en medio del proceso, como por ejemplo, a los 15 segundos, cuando vuelvas, en vez de recibir recompensa a los 60 segundos recibes a los 15 por el proceso anterior, ya que por complejidad no puedo hacer tales cosas.
        counter++;
        Debug.Log("Tiempo " + counter);
        //Tiempo entrenado.
        //if (counter >= 5)
        //if (counter >= 10)
        //if (counter >= 15)
        //if (counter >= 20)
        //if (counter >= 25)
        if (counter >= 30)
        {
            //Por si acaso
            counter = 0;
            Debug.LogWarning("Limite de ronda superado. Ganaste 1 puntos mas los acumulados por saltar por encima del zombie");
            AddReward(1f);
            //End Episode reiniciara el juego y como que intentara compilar procesos que ha hecho segun la cantidad de reward que ha recibido.
            EndEpisode();
        }
        //Entrara cada segundo que pase
        StartCoroutine(roundsTime());
    }


    public override void OnActionReceived(ActionBuffers actions)
    {

        //Debug.Log(actions.ContinuousActions[1]);
        //El array del ContinuousActions te devolvera un valor entre 0f y 1f, y el tamaño del array depende del tamaño que le hayas puesto en el behavior parameters del inspector
        transform.position += new Vector3(actions.ContinuousActions[0], 0, actions.ContinuousActions[1]) * Time.deltaTime * 15f ;
        if(actions.ContinuousActions[2]>= 0.5f)
        {
            jump(actions.ContinuousActions[2]);
        }
    }

    private void jump(float f)
    {
        this.RequestDecision();
        if (canJump)
        {
            Debug.Log("Saltando");
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, f * 10f , 0);
            canJump = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Zombie")
        {
            Debug.LogWarning("Has muerto por el zombie. -5 puntos");
            SetReward(-5f);
            EndEpisode();
        }
    }
    //Nota: el colision detector esta puesto como continuous
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "PointGenerator")
        {
            Debug.LogWarning("Salto por encima, se suman 0.5 puntos");
            AddReward(0.5f);
        }
        else
        {
            Debug.LogWarning("Muerto por caida. -10 puntos");
            //Si te caes, acabas partida, no recibes ningun punto porque no queremos que la ia sea tonta y consiga puntos cayendose al vacio. Tambien lo mismo con que te mate el zombie.
            SetReward(-10f);
            EndEpisode();
        }

    }



    private void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.name == "Base")
        {
            canJump = true;
        }
        else
        {
            canJump = false;
        }
    }

    private void FixedUpdate()
    {
        //Necesito que devuelva valores, no importa cuando sea.
        RequestDecision();
    }

}
