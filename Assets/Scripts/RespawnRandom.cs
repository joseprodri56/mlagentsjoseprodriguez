﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnRandom : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        respawn();
    }

    public void respawn()
    {
        this.transform.position = new Vector3(UnityEngine.Random.Range(-4f, 4f), this.transform.position.y, UnityEngine.Random.Range(0f, 4f));
    }

    
}
