﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPUControler : MonoBehaviour
{
    //ALERTA: NO ACTIVES ESTE SCRIPT MIENTRAS ESTA ACTIVADO EL MODO ENTRENAMIENTO. PROVOCARAS MALOS RESULTADOS EN LA CPU (del juego).

    // Update is called once per frame
    void Update()
    {
        //Lo hare manual en vez de usar los Input.GetAxis("Horizontal") y Input.GetAxis("Vertical")
        if (Input.GetKey("w"))
        {
            this.transform.position += new Vector3(0, 0, 0.5f) * Time.deltaTime * 10f;
        }
        if (Input.GetKey("s"))
        {
            this.transform.position += new Vector3(0, 0, -0.5f) * Time.deltaTime * 10f;
        }
        if (Input.GetKey("a"))
        {
            this.transform.position += new Vector3(-0.5f, 0, 0) * Time.deltaTime * 10f;
        }
        if (Input.GetKey("d"))
        {
            this.transform.position += new Vector3(0.5f, 0, 0) * Time.deltaTime * 10f;
        }
        //Alerta, no hay limitadores en el tema de salto. Solo es asignado el limitador en ScaredMan (aparte recomiendo no ponerle limitador en el controller).
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0.5f * 10f, 0);
        }

    }
}
