﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombienavmesh : MonoBehaviour
{
    GameObject CPU;
    // Start is called before the first frame update
    void Start()
    {
        //CPU es el gameobject donde tiene implementado los mlagents.
        CPU = GameObject.Find("CPU");
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<NavMeshAgent>().destination = CPU.transform.position;
    }
}
